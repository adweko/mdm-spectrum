#!/usr/bin/env bash

export TMP=${ROOTDIR}/tmp
mkdir -p ${TMP}

# set this to true if you are on Windows (win10, git bash) system. It will cygpath-convert all inputs to be Windows-java-compliant
export ON_WINDOWS=true

# each script writes resolved commands to a tmp file for the Spectrum CLI to execute. This is the tmp filename
export CLI_FILE=${TMP}/tmp.cli

# set the path to project root here for re-use in other scripts
