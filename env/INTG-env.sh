#!/usr/bin/env bash

# EDIT THESE TO YOUR NEEDS

# setup for server
export HOST="mdm-spectrum-intg.helvetia.ch"
export PORT="8443"
export USER="admin"
export PASSWORD="admin" #TODO: read from git-ignored file
# "true" or "false", depending on whether connect should use HTTPS
export SSL="true"

### DO NOT EDIT REMAINING ###

export ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd | awk -F 'env' '{print $1}')"
source ${ROOTDIR}/env/ALL-env.sh

export CONNECT_COMMAND="connect --h ${HOST}:${PORT} --u ${USER} --p ${PASSWORD} --s ${SSL}"