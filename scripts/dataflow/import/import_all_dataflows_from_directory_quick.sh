#!/usr/bin/env bash

usage() {
  [[ $1 ]] && echo "Error: $1"
  echo "imports all dataflows and exposes them"
  echo "analyzes their dependencies first and establishes an import order"
  echo "usage: $0 -e <env-file> -d <absolute-path-to-dir>"
  exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:d:" arg; do
  case $arg in
  e) # Specify env file.
    export ENV_FILE=${OPTARG}
    ;;
  d)
    export DIR=${OPTARG}
    ;;
  h | *) # Display usage and exit
    usage
    ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}
[[ -z ${DIR} ]] && usage "dir not set"

echo "importing and exposing all dataflows from directory ${DIR}"

# create import order
python ${ROOTDIR}/scripts/dataflow/util/get_import_ordering.py -d ${DIR} -o ${TMP}/import_order.dat

# import and expose

echo ${CONNECT_COMMAND} >${CLI_FILE}

cat ${TMP}/import_order.dat | while read df; do
  echo "searching $(echo "$df").df"
  dataflow="$(find ${DIR} -name "$(echo "$df").df")"
  echo "will import & expose "${dataflow}""
  # absolute path to dataflow file
  DATAFLOW_PATH=$(readlink -f "${dataflow}")
  # name of the dataflow in Spectrum, filename without extension (by convention)
  DATAFLOW_NAME=$(basename ${DATAFLOW_PATH} | sed 's@.df@@')
  SPECTRUM_FOLDER=$(echo ${DATAFLOW_PATH} | awk -F "dataflows" '{print $2}' | xargs dirname)
  if [[ ${ON_WINDOWS} == "true" ]]; then
    echo "dataflow import --f ""$(cygpath -pw ${DATAFLOW_PATH})"" --p "${SPECTRUM_FOLDER}"" >>${CLI_FILE}
  else
    echo "dataflow import --f "${DATAFLOW_PATH}" --p "${SPECTRUM_FOLDER}"" >>${CLI_FILE}
  fi
  echo "dataflow expose --d "${DATAFLOW_NAME}"" >>${CLI_FILE}
done

if [[ ${ON_WINDOWS} == "true" ]]; then
  sed -i 's@\\@\\\\@g' "${CLI_FILE}"
fi

bash ${ROOTDIR}/scripts/wrapped-cli.sh --cmdfile ${CLI_FILE}
