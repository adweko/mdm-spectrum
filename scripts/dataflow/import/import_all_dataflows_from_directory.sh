#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "imports all dataflows and exposes them"
    echo "analyzes their dependencies first and establishes an import order"
    echo "usage: $0 -e <env-file> -d <absolute-path-to-dir>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:d:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    d)
      export DIR=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}
[[ -z ${DIR} ]] && usage "dir not set"

# create import order
python ${ROOTDIR}/scripts/dataflow/util/get_import_ordering.py -d ${DIR} -o ${TMP}/import_order.dat

# import and expose

cat ${TMP}/import_order.dat | while read df; do
    dataflow=`find ${DIR} -name ${df}.df`
    echo "will import ${dataflow}"
    bash ${ROOTDIR}/scripts/dataflow/import/import_single_dataflow.sh -e ${ENV_FILE} -f `readlink -f ${dataflow}`
done