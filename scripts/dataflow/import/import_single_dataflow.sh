#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file> -f <name-of-dataflow-to-import [-x (to NOT expose)]"
    exit 0
}
[[ $# -eq 0 ]] && usage

export EXPOSE=true

while getopts ":he:f:x" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    f) # Specify dataflow to import
      export DATAFLOW=${OPTARG}
      ;;
    x) # specify -x if you do NOT want to expose the dataflow
      export EXPOSE=false
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
[[ -z ${DATAFLOW} ]] && usage "dataflow file not set"

source ${ENV_FILE}

# absolute path to dataflow file
DATAFLOW_PATH=`readlink -f ${DATAFLOW}`
# name of the dataflow in Spectrum, filename without extension (by convention)
DATAFLOW_NAME=$(basename ${DATAFLOW_PATH} | sed 's@.df@@')
SPECTRUM_FOLDER=`echo ${DATAFLOW_PATH} | awk -F "dataflows" '{print $2}' | xargs dirname`

echo ${CONNECT_COMMAND} > ${CLI_FILE}
echo "dataflow import --f ${DATAFLOW_PATH} --p ${SPECTRUM_FOLDER}" >> ${CLI_FILE}
if [[ ${EXPOSE} = true ]]; then
    echo "dataflow expose --d ${DATAFLOW_NAME}" >> ${CLI_FILE}
fi

if [[ ${ON_WINDOWS} == "true" ]]; then
  sed -i 's@\\@\\\\@g' "${CLI_FILE}"
fi


bash ${ROOTDIR}/scripts/wrapped-cli.sh --cmdfile ${CLI_FILE}