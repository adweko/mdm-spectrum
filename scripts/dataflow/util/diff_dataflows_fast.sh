#!/usr/bin/env bash

usage() {
  [[ $1 ]] && echo "Error: $1"
  echo "diff two dataflows. Check $? afterwards to see fi they are identical (0) or not (1)."
  echo "Since Spectrum sets some fields differently every time you possibly export/import the same dataflow,"
  echo "this script diffs them by pretty printing the xml representation, deleting lines with tags "
  echo "<created_by>, <created_date>, <last_modified_by> and <last_modified_date>"
  echo "and runs diff on the rest."
  echo "usage: $0 -e <env-file> -l <df file 1> -r <df file 2>"
  exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:l:r:" arg; do
  case $arg in
  e) # Specify env file.
    export ENV_FILE=${OPTARG}
    ;;
  l)
    export LEFT=${OPTARG}
    ;;
  r)
    export RIGHT=${OPTARG}
    ;;
  h | *) # Display usage and exit
    usage
    ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}
[[ -z ${LEFT} ]] && usage "left file not set"
[[ -z ${RIGHT} ]] && usage "right file not set"


xmllint --pretty 1 "${LEFT}" > ${TMP}/1.pretty
xmllint --pretty 1 "${RIGHT}" > ${TMP}/2.pretty
cat ${TMP}/1.pretty | sed '/<!--/d' | sed '/<created_by>/d' | sed '/<created_date>/d' | sed '/<last_modified_by>/d' | sed '/<last_modified_date>/d' > ${TMP}/1.clean
cat ${TMP}/2.pretty | sed '/<!--/d' | sed '/<created_by>/d' | sed '/<created_date>/d' | sed '/<last_modified_by>/d' | sed '/<last_modified_date>/d' > ${TMP}/2.clean
diff -q ${TMP}/1.clean ${TMP}/2.clean > /dev/null && exit 0 || exit 1