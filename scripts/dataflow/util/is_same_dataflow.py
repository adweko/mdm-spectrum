import argparse
import sys

from xmldiff import main

parser = argparse.ArgumentParser(description='Check whether two exported dataflows from Spectrum are logically '
                                             'identical This means the only different fields in their XML '
                                             'representation are last_modified_by, last_modified_date, '
                                             'created_date or created_by. '
                                             'Comments are ignored. '
                                             'The return code is either 1 when different or 0 when equal.'
                                             'Hint: Do not run your bash scripts with set -e if you want to do'
                                             'something based on the 1 return code.')
parser.add_argument("-f1", "--file1", type=str, help="file 1 to compare to file 2", required=True)
parser.add_argument("-f2", "--file2", type=str, help="file 2 to compare to file 1", required=True)

args = parser.parse_args()
file1 = args.file1
file2 = args.file2

diff = main.diff_files(file1, file2)
# print(diff)

allowed_node_names = ["last_modified_by", "last_modified_date", "created_by", "created_date"]

for d in diff:
    contained = True in [True for e in allowed_node_names if e in d[0]]
    if not contained:
        print(f"{file1} and {file2} are different")
        sys.exit(1)

print(f"{file1} and {file2} are logically identical")
