import argparse
import os
import sys
from collections import defaultdict


def get_list_of_files(dir_name: str):
    """
    For the given path, get the List of all files in the directory tree
    """
    # create a list of file and sub directories
    # names in the given directory
    list_of_file = os.listdir(dir_name)
    all_files = list()
    # Iterate over all the entries
    for entry in list_of_file:
        # Create full path
        full_path = os.path.join(dir_name, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(full_path):
            all_files = all_files + get_list_of_files(full_path)
        else:
            all_files.append(full_path)

    return all_files


def file_contains_string(file, string):
    with open(file) as f:
        if string in f.read():
            return True
        else:
            return False


parser = argparse.ArgumentParser(description='Creates a file containing the order in which to import dataflows into '
                                             'Spectrum, such that no dependency conflicts arise.')
parser.add_argument("-d", "--dir", type=str, help="directory in which the dataflows reside", required=True)
parser.add_argument("-o", "--out", type=str, help="output file", required=False, default="import_order.dat")

args = parser.parse_args()
directory = args.dir
out_file = args.out

paths = get_list_of_files(directory)
names = [os.path.splitext(os.path.basename(df))[0] for df in get_list_of_files(args.dir)]
names_paths = zip(names, paths)

df_to_contained_dfs = defaultdict(list)

for df in names_paths:
    (my_name, my_path) = df
    # print("contains self:", file_contains_string(my_path, my_name))
    for other_df in names:
        if my_name != other_df:
            # This is how other dataflows are referenced in the xml format of .df files
            if file_contains_string(my_path, "<value><![CDATA[" + other_df + "]"):
                # print("adding ", other_df, " to dependencies of " + my_name)
                df_to_contained_dfs[my_name].append(other_df)

unexposed = names.copy()
already_exposed = []

remaining = len(unexposed)
while len(unexposed) != 0:
    for df in unexposed:
        dependencies = df_to_contained_dfs.get(df)
        if (dependencies is None) or all(dep in already_exposed for dep in dependencies):
            already_exposed.append(df)
            unexposed.remove(df)
    if len(unexposed) == remaining:
        print("ERROR: There seems to be a cyclic dependency")
        print("exposed:", already_exposed)
        print("remaining:", unexposed)
        # print(df_to_contained_dfs["createBusinessPartner"])
        for r in unexposed:
            print(r, [a for a in df_to_contained_dfs[r] if a in unexposed])
        sys.exit(1)
    else:
        remaining = len(unexposed)

print(len(already_exposed))

with open(out_file, mode="w") as out:
    out.write('\n'.join(already_exposed))
    out.write('\n')
