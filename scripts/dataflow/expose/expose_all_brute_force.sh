#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "expose all dataflows N times"
    echo "usage: $0 -e <env-file> -n <number-cycles>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:n:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    n) # number of iterations
      export NUM_ITERATIONS=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}
[[ -z ${NUM_ITERATIONS} ]] && usage "num iterations not set"

raw_list=${TMP}/dataflows_raw.dat
all_dataflow_names=${TMP}/all_dataflow_names.dat

${ROOTDIR}/scripts/dataflow/general/list_dataflows.sh -e ${ENV_FILE} > ${raw_list}
tail -n +5 ${raw_list} | sed '/+---------*-+-*-+/d' | awk 'NF' | awk -F '|' '{print $2}' > ${all_dataflow_names}


for ((i=1; i<${NUM_ITERATIONS}; i++)); do
    echo "iteration ${i}"
    cat ${all_dataflow_names} | while read df; do
        ${ROOTDIR}/scripts/dataflow/expose/expose_single_dataflow.sh -e ${ENV_FILE} -d ${df}
    done
done
