#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file> -d <name-of-dataflow-to-unexpose>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:d:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    d) # Specify dataflow to unexpose
      export DATAFLOW=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
[[ -z ${DATAFLOW} ]] && usage "dataflow name not set"

source ${ENV_FILE}


echo ${CONNECT_COMMAND} > ${CLI_FILE}
echo "dataflow unexpose --d ${DATAFLOW}" >> ${CLI_FILE}

bash ${ROOTDIR}/scripts/wrapped-cli.sh --cmdfile ${CLI_FILE}