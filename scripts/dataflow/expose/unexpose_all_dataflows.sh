#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}

raw_list=${TMP}/dataflows_raw.dat
all_dataflow_names=${TMP}/all_dataflow_names.dat

${ROOTDIR}/scripts/dataflow/general/list_dataflows.sh -e ${ENV_FILE} > ${raw_list}
tail -n +5 ${raw_list} | sed '/+---------*-+-*-+/d' | awk 'NF' | awk -F '|' '{print $2}' > ${all_dataflow_names}


cat ${all_dataflow_names} | while read df; do
    ${ROOTDIR}/scripts/dataflow/expose/unexpose_single_dataflow.sh -e ${ENV_FILE} -d ${df}
done
