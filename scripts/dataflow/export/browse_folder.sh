#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file> [ -p <path-to-folder> (default is /) ]"
    exit 0
}
[[ $# -eq 0 ]] && usage

export FOLDER_PATH="/"

while getopts ":he:p:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    p) # Specify output directory
      export FOLDER_PATH=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}

echo ${CONNECT_COMMAND} > ${CLI_FILE}
echo "folder browse --p ${FOLDER_PATH}" >> ${CLI_FILE}

${ROOTDIR}/scripts/wrapped-cli.sh --cmdfile ${CLI_FILE}