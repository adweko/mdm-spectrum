#!/usr/bin/env bash
#set -v

usage() {
  [[ $1 ]] && echo "Error: $1"
  echo "usage: $0 -e <env-file> [ -x (if given,
     export latest saved version. default is last exposed version)] [ -c (if given, do not check for logical identity
     of already existing dataflow) ]"
  exit 0
}
[[ $# -eq 0 ]] && usage

export EXPOSED=false
CHECK_IDENTITY=true

while getopts ":he:x" arg; do
  case $arg in
  e) # Specify env file.
    export ENV_FILE=${OPTARG}
    ;;
  x) # if given, do not export exposed but latest saved dataflows
    export EXPOSED=true
    ;;
  c) # if given, do not check for logical identity
    export CHECK_IDENTITY=false
    ;;
  h | *) # Display usage and exit
    usage
    ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}

raw_list=${TMP}/dataflows_raw.dat
all_dataflow_names=${TMP}/all_dataflow_names.dat
dataflows_with_folders=${TMP}/df_with_folders.dat

${ROOTDIR}/scripts/dataflow/general/list_dataflows.sh -e ${ENV_FILE} > ${raw_list}
tail -n +5 ${raw_list} | sed '/+---------*-+-*-+/d' | awk 'NF' | awk -F '|' '{print $2}' > ${all_dataflow_names}

[[ -e ${dataflows_with_folders} ]] && rm ${dataflows_with_folders}

cat "${all_dataflow_names}" | while read name; do
  knownFolder=$(find ${ROOTDIR}/dataflows/ -name "${name}".df)
  if [[ -z ${knownFolder} ]]; then
    echo "$name /" >> ${dataflows_with_folders}
  else
    echo "$name $(echo ${knownFolder} | awk -F "dataflows" '{print $2}' | xargs dirname)" >> ${dataflows_with_folders}
  fi
done

echo ${CONNECT_COMMAND} > ${CLI_FILE}
cat ${dataflows_with_folders} | while read df; do
  dataflow=$(echo ${df} | awk '{print $1}')
  folder=$(echo ${df} | awk '{print $2}')
  echo "will export $dataflow to $folder"
  if [[ ${ON_WINDOWS} == "true" ]]; then
    echo "dataflow export --d ${dataflow} --e ${EXPOSED} --o $(cygpath -pw ${TMP})" >> ${CLI_FILE}
  else
    echo "dataflow export --d ${dataflow} --e ${EXPOSED} --o ${TMP}" >> ${CLI_FILE}
  fi
done

bash ${ROOTDIR}/scripts/wrapped-cli.sh --cmdfile ${CLI_FILE}

cat "${dataflows_with_folders}" | while read df; do
  dataflow="$(echo ${df} | awk '{print $1}')"
  folder=$(echo ${df} | awk '{print $2}')
  output_dir=${ROOTDIR}/dataflows/${folder}
  echo "will maybe copy $dataflow to $folder"
  if [[ -f ${output_dir}/"${dataflow}".df && ${CHECK_IDENTITY} == true ]]; then
    #python ${ROOTDIR}/scripts/dataflow/util/is_same_dataflow.py -f1 ${TMP}/"${dataflow}".df -f2 ${output_dir}/"${dataflow}".df
    ${ROOTDIR}/scripts/dataflow/util/diff_dataflows_fast.sh -e ${ENV_FILE} -l ${TMP}/"${dataflow}".df -r ${output_dir}/"${dataflow}".df
    RC=$?
    if [[ ${RC} -ne 0 ]]; then
      echo "are different, will overwrite"
      cp ${TMP}/"${dataflow}".df ${output_dir}
    else
      echo "are identical, do nothing"
    fi
  else
    echo "is new, create"
    cp ${TMP}/"${dataflow}".df ${output_dir}
  fi

done

