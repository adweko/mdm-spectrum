#!/usr/bin/env bash

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file> -f <name-of-dataflow-to-export> -o output-directory-absolute-path [ -x (if given,
     export latest saved version. default is last exposed version)] [ -c (if given, do not check for logical identity
     of already existing dataflow) ]"
    exit 0
}
[[ $# -eq 0 ]] && usage

export EXPOSED=false
export CHECK_IDENTITY=true

while getopts ":he:f:o:xc" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    f) # Specify dataflow to export
      export DATAFLOW=${OPTARG}
      ;;
    o) # specify output directory
      export OUTPUT_DIR=${OPTARG}
      ;;
    x) # if given, do not export exposed but latest saved dataflows
      export EXPOSED=true
      ;;
    c) # if given, do not check for logical identity
      export CHECK_IDENTITY=false
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
[[ -z ${DATAFLOW} ]] && usage "dataflow name not set"
[[ -z ${OUTPUT_DIR} ]] && echo "output dir not set, using ${PWD}" && export OUTPUT_DIR=${PWD}

source ${ENV_FILE}

echo ${CONNECT_COMMAND} > ${CLI_FILE}
if [[ ${ON_WINDOWS} == "true" ]]; then
  echo "dataflow export --d ${DATAFLOW} --e ${EXPOSED} --o $(cygpath -pw ${TMP})" >> ${CLI_FILE}
else
  echo "dataflow export --d ${DATAFLOW} --e ${EXPOSED} --o ${TMP}" >> ${CLI_FILE}
fi

bash ${ROOTDIR}/scripts/wrapped-cli.sh --cmdfile ${CLI_FILE}

if [[ -f ${OUTPUT_DIR}/${DATAFLOW}.df && ${CHECK_IDENTITY} == true ]]; then
    python ${ROOTDIR}/scripts/dataflow/util/is_same_dataflow.py -f1 ${TMP}/${DATAFLOW}.df -f2 ${OUTPUT_DIR}/${DATAFLOW}.df
    RC=$?
    if [[ ${RC} -ne 0 ]]; then
        echo "are different, will overwrite"
        cp ${TMP}/${DATAFLOW}.df ${OUTPUT_DIR}
    else
        echo "are identical, do nothing"
    fi
else
    echo "is new, create"
    cp ${TMP}/${DATAFLOW}.df ${OUTPUT_DIR}
fi

