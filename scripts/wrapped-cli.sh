#!/bin/sh
#export CLI_FOLDER="/d/dev/workspace/mdm-spectrum/spectrum-cli-19.1"

# For git bash on Win10 (java binary is Windows), use this here
#classpath=\'"`echo $(cygpath -pw ${CLI_FOLDER}/lib/*) | tr ' ' ';'`"\'

#( cd $CLI_FOLDER && java -cp ${classpath} -Djava.util.logging.config.file=${CLI_FOLDER}/logging.properties com.pb.spectrum.cli.Bootstrap $* )

# on UNIX bash (java binary is linux), use this

export CLI_FOLDER="/home/vagrant/dev/workspace/mdm-spectrum/spectrum-cli-19.1/"
( cd ${CLI_FOLDER} && java -cp ${CLI_FOLDER}/lib/spectrum-cli-runtime*.jar:${CLI_FOLDER}/lib/* -Djava.util.logging.config.file=${CLI_FOLDER}/logging.properties com.pb.spectrum.cli.Bootstrap "$@" )