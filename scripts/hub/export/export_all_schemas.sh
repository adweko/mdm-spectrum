#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}

raw_list=${TMP}/models_raw.dat
model_names=${TMP}/model_names.dat

${ROOTDIR}/scripts/hub/general/list_models.sh -e ${ENV_FILE} > ${raw_list}
tail -n +5 ${raw_list} | sed '/+---------*-+-*-+/d' | awk 'NF' | awk -F '|' '{print $2}' | awk 'NF' > ${model_names}

cat ${model_names} | while read model; do
    echo "will export $model"
     if [[ ${ON_WINDOWS} == "true" ]]; then
       ${ROOTDIR}/scripts/hub/export/export_single_schema.sh -e ${ENV_FILE} -m ${model} -o $(cygpath -pw ${ROOTDIR})/hub/model/
  else
    ${ROOTDIR}/scripts/hub/export/export_single_schema.sh -e ${ENV_FILE} -m ${model} -o ${ROOTDIR}/hub/model/
  fi
done

rm -r ${TMP}