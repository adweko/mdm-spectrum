#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file> -m <name-of-model-to-export> -o output-directory-absolute-path"
    exit 0
}
[[ $# -eq 0 ]] && usage


while getopts ":he:m:o:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    m) # Specify model for which to export schema
      export MODEL_NAME=${OPTARG}
      ;;
    o) #
      export OUTPUT_DIR=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
[[ -z ${MODEL_NAME} ]] && usage "model name not set"
[[ -z ${OUTPUT_DIR} ]] && echo "output dir not set, using ${PWD}"

source ${ENV_FILE}


echo ${CONNECT_COMMAND} > ${CLI_FILE}
if [[ -z ${OUTPUT_DIR} ]]; then
    echo "hub schema export --m ${MODEL_NAME} --f ${PWD}">> ${CLI_FILE}
else
    echo "hub schema export --m ${MODEL_NAME} --f ${OUTPUT_DIR}" >> ${CLI_FILE}
fi

bash ${ROOTDIR}/scripts/wrapped-cli.sh --cmdfile ${CLI_FILE}