#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}

echo ${CONNECT_COMMAND} > ${CLI_FILE}
echo "hub model list" >> ${CLI_FILE}

${ROOTDIR}/scripts/wrapped-cli.sh --cmdfile ${CLI_FILE}
