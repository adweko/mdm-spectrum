#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}

for model in `ls ${ROOTDIR}/hub/model/`; do
    bash ${ROOTDIR}/scripts/hub/import/import_single_model.sh -e ${ENV_FILE} -m ${ROOTDIR}/hub/model/${model}
done