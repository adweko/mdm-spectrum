#!/usr/bin/env bash
set -e

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file> -m <name-of-schema/model-to-import>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:m:" arg; do
  case $arg in
    e) # Specify env file
      export ENV_FILE=${OPTARG}
      ;;
    m) # Specify schema/model to import
      export MODEL=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
[[ -z ${MODEL} ]] && usage "model file not set"

source ${ENV_FILE}

# absolute path to model file
MODEL_PATH=`readlink -f ${MODEL}`
MODEL_NAME=$(basename ${MODEL_PATH} | sed 's@.json@@')

echo ${CONNECT_COMMAND} > ${CLI_FILE}
echo "hub schema import --m ${MODEL_NAME} --f ${MODEL_PATH}" >> ${CLI_FILE}


bash ${ROOTDIR}/scripts/wrapped-cli.sh --cmdfile ${CLI_FILE}