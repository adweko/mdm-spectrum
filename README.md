# README #

Code repository for the developments on Pitney Bowes Spectrum.

## SETUP ##
Download the spectrum client-tools (Administrationsumgebung), unpack the folder and put it into the project root,
such that there is a folder spectrum-cli-19.1 present in project root.
Point /scripts/wrapped-cli.sh to that folder by editing the first line.

## Structure ##

- dataflows
    - corresponds directly to the root ("/") folder on a Spectrum instance (or Enterprise Designer)
- hub
    - holds Data-Hub-related code
    - *model* holds models/schemata exported as json
- scripts
    - holds wrapper commands for common functionalities
    - subfolders for e.g. dataflow- or schema-related scripts
    - wrapped calls to the spectrum admin tools
- env
    - holds server setup and configuration for the Spectrum environments
    - defines path to a temporary directory where scripts may store temporary files
- requirements.txt
    - requirements (besides python3) for python
    - install via `pip3 install -r requirements.txt`

## Versionierungs- & Deploymentkonzept ###

### Zielsetzung und Ansatz
Eine typische Systemumgebung besteht aus einem Entwicklungssystem (z.B. eine lokale virtuelle Umgebung oder die DEVL),
einem Integrations- oder Stagingsystem (z.B. INTG) und einem produktiven System.
Im Kontext typischer Java-Entwicklungen erfolgt die Versionierung über das Einchecken von Code in ein VCS (version
control system, z.B. git) und das Deployment über das Auschecken und Installieren einer definierten Version des Codes
auf einem der oben genannten Systeme.

Für die Entwicklungen auf dem Spectrum-System ist dieses System mit Einschränkungen übertragbar.
Entwicklungen im Spectrum finden auf einem laufenden Server/im Enterprise Designer statt, was keine direkte Integration
 in ein externes Versionierungssystem erlaubt.

Die Integration in ein externes Versionierungssystem erfolgt darüber, dass Entwicklungen als Dateien exportiert und
eingecheckt werden können. Dieser Prozess ist im einfachsten Falle manuell, kann aber über die hier entwickelten
 Skripte weitestgehend automatisiert werden.

Die Integration in ein externes Deploymentsystem erfolgt darüber, dass die exportierten Entwicklungen auch wieder auf
einem anderen Server importiert werden können. Auch dieser Prozess erfolgt im einfachstem Falle manuell,
kann aber automatisiert werden.


#### Woraus besteht eine Spectrum-Version?
Im Kontext der Entwicklungen im NL-Transformationsprojekts besteht eine Spectrum-Installation aus
1. Einem (oder mehreren) Servern, die eine definierte Version von Spectrum laufen lassen.
    - Z.B. eine virtuelle Maschine oder die DEVL-Umgebung
2. Ein (oder mehrere) Datenmodelle im Data Hub
3. Mehreren Dataflows

### Versionierung der Entwicklungen

Eine Version der Spectrum-Entwicklungen ist definiert durch ein Set an Data-Hub-Modellen und Dataflows.
Diese können aus einer Spectrum-Entwicklungsumgebung via Enterprise Designer oder Admin Konsole exportiert werden.
Eine exportierter Dataflow (.df-Datei, XML-Format) oder ein Datenmodell (.json-Datei) können dann in ein Versionierungssystem (git) eingecheckt werden.
Um eine Version dann wiederherzustellen, werden die Dateien ausgecheckt und wieder in eine Spectrum-Installation
importiert (Enterprise Designer oder Admin Konsole).

#### Datenmodelle
Die Datenmodelle werden exportiert und als .json Datei unter *hub/model* abgelegt.
Dabei wird immer nur das Schema (Modelldefinition und Metadaten), nie irgendgearteter Inhalt exportiert.
Skripte zur Automatisierung sind unter *scripts/hub* zu finden.

##### Beispielanwendungen
- Alle Schemas einer Umgebung anzeigen
    ```bash
     bash scripts/hub/general/list_models.sh -e env/DEV-env.sh
    ```
    - Alle Schemas der Umgebung DEV sollten in der Kommandozeile ausgegeben werden.

- Alle Schemas exportieren
    ```bash
     bash scripts/hub/export/export_all_schemas.sh -e env/DEV-env.sh
    ```
    - Im Ordner hub/model sollten jetzt neue .json-Dateien angelegt worden sein bzw. bereits im VCS vorhandene
     aktualisiert worden sein. `git status hub/model` gibt eine Übersicht.
    - Neue Schemas sollten mit `git add` ins VCS integriert werden.
    - Alle (neuen und geänderten) Schemas ins lokale git committen
      `git commit hub -m <commit message>`
    - Vom lokalen ins remote repository pushen
      `git push`

- Alle Schemas importieren
    ```bash
    bash scripts/dataflow/import/import_all_dataflows_from_directory.sh -e env/DEV-env.sh -d hub/model
    ```

#### Dataflows
Dataflows sind im Spectrum mit einer definierten Ordnerstruktur angelegt. Ein Ordner im Spectrum ist weniger im Sinne eines Dateisystems als mehr im Sinne eines Tags zu verstehen.
Aufgrund eines Bugs (`folder browse` funktioniert nur auf oberster Ebene) lässt sich die Ordnerstruktur nicht
automatisiert exportieren.
Daher wird mit folgenden Konventionen gearbeitet:
- Dataflows werden im Spectrum normal in Ordnern angelegt
- Beim ersten Export müssen diese manuell in den richtigen Ordner unter *dataflows* verschoben werden.
- Beim automatisierten Export wird im dataflows-Ordner gesucht, ob es schon einen Unterordner mit einem Dataflow
  gleichen Namens (immer einzigartig) gibt.
    - Wenn ja, wird dieser überschrieben
    - Wenn nicht, wird der Dataflow ohne Ordner angelegt
- Der automatisierte Import benutzt die im VCS unter *dataflows* vorhandene Ordnerstruktur
- Es wird immer die letzte exposed Version eines Dataflows exportiert (default-Verhalten der export-Skripte)

Generell hängt das Verfahren, wie am besten exportiert wird, von der Entwicklungsumgebung ab. Wird in einer lokalen VM
entiwckelt, sind alle Änderungen von einem selbst durchgeführt worden und der Export aller Dataflows entspricht genau
dem Stand der Neuentwicklungen. Auf einem geteilten Entwicklungssystem (DEVL) ist dies nicht der Fall. Hier enthält der
Export aller Dataflows den derzeitigen Stand auch der Enwicklungen anderer Entiwckler. Es könnte in diesem Fall besser
sein, immer nur die Dataflows zu exportieren, an denen man selber entwickelt hat (über *scripts/dataflow/export/export_single_dataflow.sh*) und diese ins Repository einzupflegen.

Beim "expose" und "export" von Dataflows werden einige Zeitstempel gesetzt, ohne dass sich etwas an Inhalt oder Funktion
geändert hat. Auf dem virtuellen Testsystem waren diese Zeitstempel teils in falscher Reihenfolge und unzuverlässig.
Mit den hier zur Verfügung gestellten Tools wird überprüft, ob es tatsächliche Änderung gab und keine neue Version
ins Repository aufgenommen wenn nicht.

Beim importieren und exposen der Dataflows ist es wesentlich, die richtige Reihenfolge einzuhalten. Diese ist dadurch
gegeben, dass manche Dataflows von anderen abhängen. Dann müssen die Abhängigkeiten zuerst importier und exposed werden.
Das Skript *scripts/dataflows/util/get_import_ordering.py* etabliert die Reihenfolge. Es wird automatisch ausgeführt,
wenn Dataflows über *scripts/dataflow/import/import_all_dataflows_from_directory.sh* importiert werden.

##### Beispielanwendungen
- Alle dataflows exportieren
    ```bash
    bash scripts/dataflow/export/export_all_dataflows_quick.sh -e env/DEV-env.sh 
    ```
    Dies kann etwas dauern, da alle aktualisierten dataflows über einen Vergleich der XMLs geprüft werden, ob tatsächliche Änderungen stattfanden (20 min oder mehr).
    
- Überprüfen, welche dataflows beim export geändert/neu angelegt wurden
    ```bash
    git status dataflows/
    ```
- Neue dataflows adden, committen und pushen
    - Nur einzelne (z.B. eigene Entwicklungen dataflow1 und MDMS/awesomeFlow)
    ```bash
    git add dataflows/dataflow1.df
    git commit dataflows/dataflow1.df      
    git add dataflows/MDMS/awesomeFlow.df
    git commit dataflows/MDMS/awesomeFlow.df
    git push
    ```
    - Alle neuen dataflows adden, alle neuen und geänderten committen und pushen
    ```bash
    git add dataflows/
    git commit datflows/
    git push dataflows/  
    ```

- Alle dataflows in richtiger Reihenfolge importieren und exposen
    ```bash
    bash scripts/dataflow/import/import_all_dataflows_from_directory.sh -e env/DEV-env.sh -d `readlink -f dataflows`
    ```

### Deployment

- Nur getaggte Versionen sollten deployed werden.
- Siehe https://git-scm.com/book/en/v2/Git-Basics-Tagging für die Kommandos, v.a. Abschnitt "Sharing Tags"

### Semantische Versionierung
Eine Versionsnummer besteht aus drei Teilen: MAJOR.MINOR.PATCH
TODO: Abklärung Definition offen:
- Jede Änderung des Datenmodells ist ein neues Major-Release
- REST-API-ändernde Änderungen and Dataflows sind Major-Releases
- Neue Dataflows oder Änderungen an Dataflows, ohne dass die REST-APIs betroffen sind, sind Minor-Releases
- Interne bug-fixes an Dataflows sind Patch-Releases


## TODO

- all-scripts should not connect every time. exchange loop with single script
- include a prefix DEV and throw them all into dataflows/dev folder

- show differences between two commits ``git show --pretty="format:" --name-only 690efdd772fe51d20369fe13f68c5f9a7af17fdd..388bd261d89b4726a231ba5ceeb15ab00a10bd08``
    - to find out which dataflows should be imported into already running system (set up from first commit or last deployment)
